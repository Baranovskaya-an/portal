#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
#include <time.h>
#include "cgi.h"

#define CACHEFILE "/var/www/cache/hash.csv"
#define CACHEFILEHELP "/var/www/cache/fileNew.txt"
#define CACHEPATH "/var/www/cache/"
#define FILE_EXE "/cgi-bin/answer"
#define ATTEMPT_COUNT 3
#define TIME_DEFAULT "100"
#define EMPTY ""
#define SESSION_NAME "attempt"
#define HEADER_NAME "Ответ"
#define KEY "key"
#define WORD "word"
#define USERTIME "userTime"
#define NUMBER_0 "0"
#define NUMBER_1 "1"
#define INCORRECT_DATA "Некорректные данные!!!"
#define ENTER_DATA "Введите данные"
#define ATTEMPTS "Осталось попыток: "
#define LIMIT_ATTEMPTS "Вы превысили количество попыток!!!"
#define POST "POST"
#define STRING_SIZE 512

 //Перестановка символов в обратном порядке
void reverse(char s[])
{
	int i, j;
	char c;

	for (i = 0, j = strlen(s)-1; i<j; i++, j--) 
	{
		 c = s[i];
		 s[i] = s[j];
		 s[j] = c;
	}
}
 
 //Переевод числа в строку
void itoa(int n, char s[])
{
    int i, sign;
 
    if ((sign = n) < 0)
		n = -n;          
    i = 0;
    do 
    {   
		s[i++] = n % 10 + '0';  
    } 
    while ((n /= 10) > 0);
    if (sign < 0)
        s[i++] = '-';
    s[i] = '\0';
    reverse(s);
 }
 
 //Заголовок документа
void header(char * title)
{
	printf("<!DOCTYPE html>");
	printf("<html>");
	printf("<head>");
	printf("<meta http-equiv='content-type' content='text/html; charset=utf-8'>");
	//Обнуление автоматических настроек браузера для изображения страницы и загрузка собственных 
	printf("<link rel='stylesheet' href='/reset.css'>");
	printf("<link rel='stylesheet' href='/style.css'>");
	printf("<title>");
	printf("%s", title);
	printf("</title>");
	printf("</head>");
	printf("<body>");
	
}
 //Последние закрывающие теги
void footer()
{
	printf("</body>");
	printf("</html>");
}

 //Очистка файла сообщений от устарелых данных
void cleanData()
{
	char keySave[STRING_SIZE];  
    char wordSave[STRING_SIZE];
    char timeSave[STRING_SIZE];
    char timeNow[STRING_SIZE];
    sprintf(timeNow, "%d", (int)time(NULL));
	FILE * fd = fopen(CACHEFILE, "r");
	FILE * fdNew = fopen(CACHEFILEHELP, "a");
	fscanf(fd, "%s\t%s\t%[^\n]s\n", timeSave, keySave, wordSave);
	while (feof (fd) == 0)
	{
		if (strcmp(timeSave, timeNow) > 0)
		{
			fprintf(fdNew, "%s\t%s\t%s\n", timeSave, keySave, wordSave);
		}
		fscanf(fd, "%s\t%s\t%[^\n]s\n", timeSave, keySave, wordSave);	
	}
	fclose(fdNew);
	fclose(fd);
	remove (CACHEFILE);
	rename(CACHEFILEHELP, CACHEFILE);
}

 //Сохранение нового сообщения
void saveData(char * userTime, char * key, char * word)
{
	FILE * fd = fopen(CACHEFILE, "a");
	fprintf(fd, "%d\t%s\t%s\n", (int)(time(NULL) + atoi(userTime)), key, word);
	fclose(fd);
}

 //Поиск сообщения
void findData(char * key, char * word)
{
	//Перед поиском выполняем очистку устаревших данных
	cleanData();
	
	FILE * fd = fopen(CACHEFILE, "r"); 
    char keySave[STRING_SIZE];  
    char wordSave[STRING_SIZE];
    char timeSave[STRING_SIZE]; 
    strcpy(word, EMPTY);
        
  	while (feof (fd) == 0)
	{
		fscanf(fd, "%s\t%s\t%[^\n]s", timeSave, keySave, wordSave);
		if (strcmp(key, keySave) == 0)
		{
			strcpy(word, wordSave);
			break;
		}	
	}
	fclose(fd);
}

 //Вывод страницы
void form(char * action, char * method, char * key, char * word, char *  userTime)
{
	printf("<div class='form-style-6'>");
	printf("<h1>Eltex</h1>");
	printf("<form action=%s method=%s>", action, method);
	printf("<legend><span class='number'>*</span> Сообщение</legend>");
	printf("<input type='text' name='word' placeholder='Ваше сообщение' value='%s'>", word);
	printf("<input type='text' name='userTime' placeholder='Время жизни сообщения в секундах' value='%s'>", userTime);
	printf("<input type='submit' value='Сохранить'>");
	printf("</form>");
	printf("<form action=%s method=%s>", action, method);
	printf("<legend><span class='number'>*</span> Код</legend>");
	printf("<input type='text' name='key' placeholder='Секретный код' value='%s'>", key);
	printf("<input type='submit' value='Прочитать'>");
	printf("</form>");
	printf("</div>");
}

 //Определение значений ключа и сообщения
void keyWord(char * keyNew, char * wordNew, char * userTimeNew)
{
	char * key;
	char * word;
	char * userTime;
	key = cgi_param(KEY);
	word = cgi_param(WORD);
	userTime = cgi_param(USERTIME);	
	
	char str[STRING_SIZE];
	char bufer[STRING_SIZE];
	if ((key == NULL) && (word == NULL))
	{
		//Открытие страницы
		key = malloc(sizeof(char) * STRING_SIZE);
		word = malloc(sizeof(char) * STRING_SIZE);
		userTime = malloc(sizeof(char) * STRING_SIZE);
		strcpy(key, EMPTY);
		strcpy(word, EMPTY);
		strcpy(userTime, EMPTY);
	}
	else
	{
		//Запрос на сохранение сообщения
		if ((word != NULL) && (userTime != NULL))
		{
			//Создание хэша сообщения
			unsigned char md5digest[MD5_DIGEST_LENGTH];
			MD5((unsigned char *)word, strlen(word), (unsigned char *)md5digest);
			for (int i = 0; i < MD5_DIGEST_LENGTH; i++) 
			{
				sprintf(str,"%02x",md5digest[i]);
				strcpy(bufer + i, str);
			}
			key = malloc(sizeof(char) * STRING_SIZE);
			strcpy(key, bufer);
			saveData(userTime, key, word);
		}
		else
		{
			//Запрос на прочтение сообщения
			if (key != NULL)
			{
				word = malloc(sizeof(char) * STRING_SIZE);
				userTime = malloc(sizeof(char) * STRING_SIZE);
				strcpy(userTime, TIME_DEFAULT);
				strcpy(bufer, EMPTY);
				if (cgi_session_var_exists(SESSION_NAME))
				{
					char * at = cgi_session_var(SESSION_NAME);
					if (atoi(at) + 1 < ATTEMPT_COUNT)
					{
						findData(key, bufer);
					}
				}
				else
				{
					findData(key, bufer);
				}
				
				if (strcmp(bufer, EMPTY) == 0)
				{
					//Защита от угадывания пароля
					free(key);
					key = malloc(sizeof(char) * STRING_SIZE);
					if (cgi_session_var_exists(SESSION_NAME))
					{
						char * att = cgi_session_var(SESSION_NAME);
						if (atoi(att) + 1 >= ATTEMPT_COUNT)
						{
							strcpy(word, LIMIT_ATTEMPTS);
							strcpy(key, LIMIT_ATTEMPTS);
						}
						else
						{
							itoa(atoi(att) + 1, att);
							cgi_session_alter_var(SESSION_NAME, att);
							itoa(ATTEMPT_COUNT - atoi(att), att);
							strcpy(word, INCORRECT_DATA);
							strcpy(key, ATTEMPTS);
							key = strcat(key, att);	
						}
					}
					else
					{
						cgi_session_register_var(SESSION_NAME, NUMBER_1);
						strcpy(word, INCORRECT_DATA);
						strcpy(key, ATTEMPTS);
						char k[STRING_SIZE];
						itoa(ATTEMPT_COUNT - 1, k);
						key = strcat(key, k);
					}
				}
				else
				{
					if (cgi_session_var_exists(SESSION_NAME))
					{
						cgi_session_alter_var(SESSION_NAME, NUMBER_0);
					}
					strcpy(word, bufer);
				}
			}
			else
			{
				key = malloc(sizeof(char) * STRING_SIZE);
				word = malloc(sizeof(char) * STRING_SIZE);
				userTime = malloc(sizeof(char) * STRING_SIZE);
				strcpy(key, ENTER_DATA);
				strcpy(word, ENTER_DATA);
				strcpy(userTime, TIME_DEFAULT);
			}
		}	
	}

	strcpy(keyNew, key);
	strcpy(wordNew, word);
	strcpy(userTimeNew, userTime);
	
	free(word);
	free(key);
	free(userTime);
}

 //Тело документа
void body()
{
	char * key = malloc(sizeof(char) * STRING_SIZE);
	char * word = malloc(sizeof(char) * STRING_SIZE);
	char * userTime = malloc(sizeof(char) * STRING_SIZE);
	
	keyWord(key, word, userTime);
	form(FILE_EXE, POST, key, word, userTime);
	
	free(word);
	free(key);
	free(userTime);
}

int main(int argc, char **argv)
{
	cgi_display_errors = 0;
	cgi_init();
	cgi_session_save_path(CACHEPATH);
	cgi_session_start();
	cgi_process_form();
	
	//Уничтожаем сессию, если запускается новая страница
	if ((cgi_param(KEY) == NULL) && (cgi_param(WORD) == NULL))
		{
			cgi_session_destroy();
		}
	cgi_init_headers();
		
	header(HEADER_NAME);
	body();
	footer();
	
	cgi_end();
	return 0;
}

