CGIBIN_PATH=/usr/lib/cgi-bin
HEADERS_PATH=/usr/local/include/libcgi
HTML_PATH=/var/www
LIB_PATH=/usr/local/lib/$(shell uname -i)-linux-gnu

all:install-lib copy answer

answer:
	sudo gcc -std=c99 -I$(HEADERS_PATH) cgi-bin/answer.c -o $(CGIBIN_PATH)/answer -lcgi -lcrypto

copy:
	sudo cp www/html/reset.css $(HTML_PATH)/html
	sudo cp www/html/style.css $(HTML_PATH)/html
	sudo mkdir $(HTML_PATH)/cache
	sudo chmod 777 $(HTML_PATH)/cache

install-lib:
	cd libcgi-master/build && sudo cmake .. && sudo make && sudo make install
	sudo touch /etc/ld.so.conf.d/libcgi.conf
	sudo chmod 666 /etc/ld.so.conf.d/libcgi.conf
	sudo echo $(LIB_PATH) > /etc/ld.so.conf.d/libcgi.conf
	sudo ldconfig

clean:
	sudo rm -f $(CGIBIN_PATH)/answer
